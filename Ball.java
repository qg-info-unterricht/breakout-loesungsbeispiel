import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Ball here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ball extends Actor
{
    private static GreenfootImage brickExtraHell = new GreenfootImage("brick_extra_hell.png");
    private int dx;
    private int dy;

    // Konstruktor des Balls
    public Ball() {
        // Zufallszahl zwischen -10 und 10
        this.dx = 10 - Greenfoot.getRandomNumber(21);
        this.dy = -5 - Greenfoot.getRandomNumber(6);

    }

    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        setLocation(getX() + dx, getY() + dy);

        // Reflexion oben
        if( (getY() <= 8) ) {
            this.dy = -this.dy;
        }
        // Refexion links und rechts
        if ( (getX() <= 8 ) || (getX() >= 632 )) {
            this.dx = -this.dx;
        }

        if (getOneIntersectingObject(Paddel.class) != null) {
            this.dy = -this.dy;

            // "Drall" durchs Paddel
            if(Greenfoot.isKeyDown("right") ) {
                this.dx = this.dx + 1;
            } 
            if(Greenfoot.isKeyDown("left") ) {
                this.dx = this.dx - 1;    
            }

        } 

        if (getOneIntersectingObject(Block.class) != null) {
            reflectOnBlock();
        }

        // Ball verschwindet unten...
        if ( getY() > 472 ) {
            // Zugriff auf das Spielfeld Objekt
            Spielfeld spielfeld = (Spielfeld) getWorld();
            spielfeld.setNumBalls(spielfeld.getNumBalls()-1);
            spielfeld.writeText(Integer.toString(spielfeld.getNumBalls()),3 ,2 ,10);
            spielfeld.drawNumBalls(spielfeld.getNumBalls());
            spielfeld.removeObject(this);
        }
    }  

    private void reflectOnBlock() {
        Actor block = getOneIntersectingObject(Block.class);
        Spielfeld spielfeld = (Spielfeld) getWorld();

        if (block != null) {
            dy = -dy;
            // Das Actor Objekt in ein Clock Objekt casten
            Block myblock = (Block) block;
            
            // Verlangsamender Block?
            if (myblock.slowit) {
                if ( Math.abs(dx) > 4 && Math.abs(dy) > 4) {
                    dy = (int) Math.floor((float) dy * 0.8);
                    dx = (int) Math.floor((float) dx * 0.8);
                }
            }
            
            // Hits abziehen
            myblock.hits--;
            if (myblock.hits == 0) {
                getWorld().removeObject(block);
                spielfeld.playerPoints = spielfeld.playerPoints + myblock.points;
                spielfeld.writeText(Integer.toString(spielfeld.playerPoints), 580,2,59); 
            } else {
                // Hellgrauen Block als Bild setzen
                myblock.setImage(brickExtraHell);
            }
        }
    }

}
