import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Write a description of class Spielfeld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Spielfeld extends World
{
    // wieviele Blockreiehn
    private int brickRows;
    // wieviele Baelle
    private int numBalls;
    private int maxBalls;
    // Gespielte Baelle
    private int playedBalls;
    // Punktestand
    public int playerPoints;

    
    /**
     * Constructor for objects of class Spielfeld.
     * 
     */
    public Spielfeld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(640, 480, 1);
        this.brickRows = 7;
        this.numBalls = this.maxBalls = 4;
        this.playedBalls = 0;
        this.playerPoints = 0;
        prepare();
    }

    /**
     * Bereite die Welt für den Programmstart vor.
     * Das heißt: Erzeuge die Anfangs-Objekte und füge sie der Welt hinzu.
     */
    private void prepare()
    {
        Paddel paddel = new Paddel();
        addObject(paddel,320,468);
        for( int z = 0; z<this.brickRows; z++) {
            for( int i = 32; i<640; i=i+64) {
                addObject(new Block(),i,z* 26 + 38);
            }
        }
        this.writeText(Integer.toString(this.numBalls), 3,2,10);
        this.drawNumBalls(this.numBalls);
        writeText(Integer.toString(this.playerPoints), 580,2,59); 
        addObject(new Message(4), 542, 12);

    }

    
    
    public void drawNumBalls(int num) {
        for(int i=0; i<num;i++) {
            addObject(new Message(2), i*20+28, 12);
        }
        addObject(new Message(3), num*20+28, 12);
    }

    // Schreibe einen Text an die Position x,y Breite width
    public void writeText(String text, int xPos, int yPos, int width) {
        this.getBackground().setColor(Color.BLACK);
        this.getBackground().fillRect(xPos,yPos,width,22);
        this.getBackground().setColor(Color.WHITE);
        this.getBackground().setFont(new Font("", true, false, 16));
        this.getBackground().drawString(text, xPos, yPos+16);
    }

    // Liefert die Zahl der Baelle zurueck
    public int getNumBalls() {
        return this.numBalls;
    }

    // Setzt die Ballanzahl ausf count
    public void setNumBalls(int count) {
        this.numBalls = count;
    }

    public void act() {
        int numBlocksLeft = getObjects(Block.class).size();

        // Neuen Ball auf das Paddel ablegen, wenn die Space Taste gedrueckt wird
        if("space".equals(Greenfoot.getKey()) && this.numBalls > 0 && this.playedBalls < this.maxBalls ) {
            // Neues Objekt vom Typ Ball erzeugen            
            Ball ball = new Ball();
            // Zugriff auf das Paddel-Objekt, Referenz wird gespeichert 
            // in der Variablen paddle des Typs Paddel.
            // Weil es mehrere Padel geben koennte, muss man 
            // das erste Paddel auswaehlen, das getObjects in der Liste
            // zurueckliefert.
            Paddel paddel = getObjects(Paddel.class).get(0);
            // Jetzt kann man über "paddel" auf die Methoden und Attribute des Paddels
            // zugreifen, wir holen und X und Y Koordinate des Paddels
            int paddelX = paddel.getX();
            int paddelY = paddel.getY();
            // Jetzt kann man das Ball Objekt auf das Paddel platzieren:
            addObject(ball, paddelX, paddelY - 12);
            // gespielte Baelle erhoehen
            this.playedBalls++;
        }

        // Keine Baelle mehr?
        if ( this.numBalls == 0 ) {

            if ( numBlocksLeft > 0 ) {
                // Wenn Bloecke uebrig sind: verloren!
                Message message = new Message(0);
                addObject(message, 320, 260);
            } else {
                // Keine Bloecke mehr? Gewonnen!
                Message message = new Message(1);
                addObject(message, 320, 260);
            }
            // Greenfoot stoppen
            Greenfoot.stop();

        }

        // Keine Bloecke mehr?
        if ( numBlocksLeft == 0 ) {
            // Alle Baelle zerstoeren
            removeObjects(getObjects(Ball.class));
            // Ballzahl auf Null
            this.numBalls = 0;
            // Siegesmeldung!
            Message message = new Message(1);
            addObject(message, 320, 260);
            // Greenfoot stoppen
            Greenfoot.stop();
        }

    }
}
