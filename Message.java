import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Message here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Message extends Actor
{
    private static GreenfootImage youWin = new GreenfootImage("you_win.png");
    private static GreenfootImage youLose = new GreenfootImage("game_over_you_lose.png");
    private static GreenfootImage ball = new GreenfootImage("ball.png");
    private static GreenfootImage delete_ball = new GreenfootImage("delete_ball.png");
    private static GreenfootImage score = new GreenfootImage("score.png");
    
    
    public Message (int whatMessage) {
        if ( whatMessage == 0) {
            setImage(youLose);
        }
        if ( whatMessage == 1) {
            setImage(youWin);
        } 
        if ( whatMessage == 2) {
            setImage(ball);
        }
        if ( whatMessage == 3) {
            setImage(delete_ball);
        } 
        if ( whatMessage == 4) {
            setImage(score);
        }
        
        
    }
    /**
     * Act - do whatever the Message wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
