import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Block here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Block extends Actor
{

   private static GreenfootImage brickGreen = new GreenfootImage("brick_green.png");
   private static GreenfootImage brickPurple = new GreenfootImage("brick_purple.png");
   private static GreenfootImage brickYellow = new GreenfootImage("brick_yellow.png");
   private static GreenfootImage brickRed = new GreenfootImage("brick_red.png");
   private static GreenfootImage brickExtra = new GreenfootImage("brick_extra.png");  
   private static GreenfootImage brickExtraHell = new GreenfootImage("brick_extra_hell.png");
   
   // Wieviele Treffer?
   public int hits;
   public int points;
   public boolean slowit;
   
   public Block () {
       int color =  Greenfoot.getRandomNumber(5);
        
        if ( color == 0 ) {
            setImage(brickGreen);
            this.points = 10;
            this.hits = 1;
            this.slowit = true;
        }

        if ( color == 1 ) {
            setImage(brickPurple);
            this.points = 20;
            this.hits = 1;
            this.slowit = false;
        }

        if ( color == 2 ) {
            setImage(brickYellow);
            this.points = 5;
            this.hits = 1;
            this.slowit = false;
        }

        if ( color == 3 ) {
            setImage(brickRed);
            this.points = 30;
            this.hits = 1;
            this.slowit = false;
        } 
        if ( color == 4 ) {
            setImage(brickExtra);
            this.points=100;
            this.hits = 2;
            this.slowit = false;
        }
        
        
   }

    /**
     * Act - do whatever the Block wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        
        // Add your action code here.
    }    
    
}
